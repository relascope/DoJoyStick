//
// Created by guru on 29.10.23.
//

#ifndef DOJOYSTICK_SRC_DOJOYSTICK_PLUGIN_CPP_DOJOYSTICK_PLUGIN_H_
#define DOJOYSTICK_SRC_DOJOYSTICK_PLUGIN_CPP_DOJOYSTICK_PLUGIN_H_

#include "DistrhoPlugin.hpp"
#include "dojoystick-settings.h"
#include "joystick-midi-mediator.h"
#include <cmath>
#include <extra/RingBuffer.hpp>
#include <iostream>


class DoJoyStickPlugin final
    : public Plugin,
      public dojoystick::MidiObserver {

private:
    std::shared_ptr<dojoystick::JoystickMidiMediator> joystickMidiMediator;
    HeapRingBuffer heapRingBuffer;
    static constexpr const int kMaxMidiNoteNumber = 127;

public:
    DoJoyStickPlugin()
        : Plugin(static_cast<uint32_t>(dojoystick::Parameters::kParameterCount), 0, 0) {

        std::cout << "DoJoyStickPlugin. params " << dojoystick::Parameters::kParameterCount << '\n';

        settings.velocity = 111;
        settings.joyStickIndex = 0;
        heapRingBuffer.createBuffer(8192);

        joystickMidiMediator = std::make_shared<dojoystick::JoystickMidiMediator>(settings);
        joystickMidiMediator->addObserver(this);

        joystickMidiMediator->startThread();
    }

    ~DoJoyStickPlugin() override {
        joystickMidiMediator->removeObserver(this);
        if (joystickMidiMediator->isThreadRunning()) {
            joystickMidiMediator->signalThreadShouldExit();
            joystickMidiMediator->stopThread(0);
        }
    }

    [[nodiscard]] const dojoystick::Settings &getSettings() const { return settings; }

    void onMidiEvent(const MidiEvent &midiEvent) override {
        std::cout << "MidiEvent in \n"
                  << std::flush;
        heapRingBuffer.writeCustomType(midiEvent);
        heapRingBuffer.commitWrite();
    }

protected:
    [[nodiscard]] const char *getLabel() const override { return "DoJoyStick"; }

    [[nodiscard]] const char *getDescription() const override { return "Joystick to DoJoy music. UseCase: MidiController (e.g. versatile foot drum)"; }

    [[nodiscard]] const char *getMaker() const override { return "DoJoy.at"; }

    [[nodiscard]] const char *getLicense() const override { return "AGPL3"; }

    [[nodiscard]] uint32_t getVersion() const override { return d_version(1, 0, 1); }

    [[nodiscard]] int64_t getUniqueId() const override { return d_cconst('D', 'J', 'o', 'y'); }


    void initParameter(uint32_t index, Parameter &parameter) override {
        if (index == dojoystick::Parameters::kVelocity) {
            parameter.name = "velocity";
            parameter.symbol = "gain";// TODO
            parameter.hints |= kParameterIsInteger;
            parameter.ranges.def = 108;
            parameter.ranges.min = 0;
            parameter.ranges.max = 127;
        } else if (index < dojoystick::Parameters::kParameterCount) {
            parameter.name = String("MidiNote for Button ") += String(index);
            parameter.symbol = String("BTN_MIDI") += String(index);
            parameter.hints |= kParameterIsInteger;
            parameter.ranges.def = 60;
            parameter.ranges.min = 0;
            parameter.ranges.max = kMaxMidiNoteNumber;
        }
    }


    [[nodiscard]]

    float
    getParameterValue(uint32_t index) const override {
        if (index == dojoystick::Parameters::kVelocity) return settings.velocity;

        if (index < dojoystick::Parameters::kParameterCount) return settings.midiNoteForButtonNumber[index];

        return 0;
    }


    void setParameterValue(uint32_t index, const float value) override {
        if (index == dojoystick::Parameters::kVelocity) settings.velocity = static_cast<uint8_t>(value);
        if (index < dojoystick::Parameters::kParameterCount) settings.midiNoteForButtonNumber[index] = static_cast<uint8_t>(std::round(value));
    }


    void run(const float **inputs, float **outputs, const uint32_t frames) override {

        if (!heapRingBuffer.isDataAvailableForReading()) return;

        for (size_t frame_idx = 0; frame_idx < frames && heapRingBuffer.isDataAvailableForReading(); ++frame_idx) {
            MidiEvent midiEvent{};
            if (!heapRingBuffer.readCustomType(midiEvent)) continue;

            midiEvent.frame = frame_idx;

            writeMidiEvent(midiEvent);
        }
    }

private
    :
    dojoystick::Settings settings;

    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DoJoyStickPlugin);
};
#endif//DOJOYSTICK_SRC_DOJOYSTICK_PLUGIN_CPP_DOJOYSTICK_PLUGIN_H_
