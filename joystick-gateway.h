#ifndef JSEVENT_H
#define JSEVENT_H

#include "joystick-event.h"
#include "joystick-observer.h"
#include <atomic>
#include <list>

namespace dojoystick {
    /**
 * @brief The LinuxJoystickGateway class
 * Uses SDL2 to read Joystick events
 * Reads Joystick events and propagates through JoystickObserver
 */
    class JoystickGateway {
    public:
        explicit JoystickGateway() = default;
        virtual void startEventLoop() = 0;
        virtual ~JoystickGateway() {
            stop();
        };

        void stop() { shouldRun = false; }
        void addJoystickObserver(JoystickObserver *observer) { observers.push_back(observer); }

    protected:
        void notifyObservers(const JoystickEvent &event) const {
            for (const auto ob: observers) { ob->onJoystickEvent(event); }
        };
        std::atomic<bool> shouldRun = true;

    private:
        std::list<JoystickObserver *> observers;
    };
}// namespace dojoystick
#endif
