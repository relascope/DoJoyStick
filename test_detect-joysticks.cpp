//
// Created by guru on 02.12.23.
//


#ifdef __linux__
#include <linux/joystick.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#endif

#include <iostream>


#ifdef COMPILE_WITH_SDL2
#include <SDL.h>
#include <SDL_joystick.h>
void printFirstJoyStickSDL() {

    if (SDL_Init(SDL_INIT_GAMECONTROLLER)) { return; }

    SDL_Joystick *joy;
    const int sdl_num_joysticks = SDL_NumJoysticks();
    std::cout << "Number of Joysticks: " << sdl_num_joysticks << std::endl;

    if (sdl_num_joysticks > 0) {
        // Open joystick
        joy = SDL_JoystickOpen(0);

        if (joy) {
            printf("Opened Joystick 0\n");
            printf("Name: %s\n", SDL_JoystickName(joy));
            printf("Number of Axes: %d\n", SDL_JoystickNumAxes(joy));
            printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(joy));
            printf("Number of Balls: %d\n", SDL_JoystickNumBalls(joy));
            SDL_JoystickClose(joy);
        } else printf("Couldn't open Joystick 0\n");
    }
}
# endif


#ifdef __linux__
void printFirstJoyStickLinux() {
    int joy_fd;
    __u8 buttons;

    const auto deviceName = "/dev/input/js0";
    if ((joy_fd = open(deviceName, O_RDONLY)) < 0) {
        fprintf(stderr, "Unable to open device ");
        perror(deviceName);
    }

    char name[256] = "Undefined";
    ioctl(joy_fd, JSIOCGBUTTONS, &buttons);
    ioctl(joy_fd, JSIOCGNAME(128), name);

    printf("Joystick %s with %i buttons\n", name, buttons);
}
#endif

int main() {

#ifdef COMPILE_WITH_SDL2
    std::cout << "===SDL" << std::endl;
    printFirstJoyStickSDL();
#endif

#ifdef __linux__
    std::cout << "===Linux" << std::endl;
    printFirstJoyStickLinux();
#endif

    return 0;
}
