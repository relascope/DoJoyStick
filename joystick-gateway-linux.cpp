#ifdef __linux__
#include "joystick-gateway-linux.h"
#include "joystick-gateway.h"

#include <iostream>
#include <string>

#include <fcntl.h>
#include <linux/joystick.h>
#include <sys/ioctl.h>
#include <unistd.h>

namespace dojoystick {

    JoystickGatewayLinux::JoystickGatewayLinux(const uint8_t joystickIndex) : joystickIndex(joystickIndex), joy_fd(-1) {
        openJoystick(joystickIndex);
    }

    bool JoystickGatewayLinux::openJoystick(const uint8_t joystickIndex) {
        std::string deviceName = "/dev/input/js" + std::to_string(joystickIndex);

        joy_fd = open(deviceName.c_str(), O_RDONLY);

        if (joy_fd < 0) {
            std::cerr << "Unable to open device " << deviceName << '\n';
            return false;
        }

        char fullJoystickName[256] = "Undefined";
        int buttons;

        ioctl(joy_fd, JSIOCGNAME(128), fullJoystickName);
        ioctl(joy_fd, JSIOCGBUTTONS, &buttons);

        std::cout << "Opened device " << deviceName << " with name " << fullJoystickName << "\n and " << buttons << " buttons\n";

        return true;
    }

    void JoystickGatewayLinux::startEventLoop() {
        struct js_event jsEvent {
            0
        };

        while (this->shouldRun && joy_fd >= 0) {
            if (read(joy_fd, &jsEvent, sizeof(struct js_event)) != sizeof(struct js_event)) {
                std::cerr << "Unable to read joystick event\n";
                continue;
            }

            if (jsEvent.type == JS_EVENT_BUTTON) {
                JoystickEvent event{true, 0};
                event.buttonDown = jsEvent.value == 1;
                event.buttonNumber = static_cast<uint8_t>(jsEvent.number);

                notifyObservers(event);
            }
        }
    }

    void JoystickGatewayLinux::closeJoystick() {
        if (joy_fd < 0) return;

        const int res = close(joy_fd);
        if (res < 0) {
            std::cerr << "Unable to close joystick\n";
        }
    }


    JoystickGatewayLinux::~JoystickGatewayLinux() {
        closeJoystick();
    }
}// namespace dojoystick
#endif