//
// Created by guru on 19.12.23.
//

#ifndef JOYSTICK_GATEWAY_FACTORY_H
#define JOYSTICK_GATEWAY_FACTORY_H

#include "joystick-gateway-linux.h"
#include "joystick-gateway-windows.h"

namespace dojoystick {
    class JoystickGateway;

    class JoystickGatewayFactory {
    public:
        static std::unique_ptr<JoystickGateway> create(uint8_t joystickIndex) {
#ifdef __linux__
            return std::unique_ptr<JoystickGateway>(new JoystickGatewayLinux(joystickIndex));
#endif

#if defined(_WIN64) || defined(_WIN32) || defined(__CYGWIN__)

            return std::unique_ptr<JoystickGateway>(new JoystickGatewayWindows(joystickIndex));
#endif

            return std::unique_ptr<JoystickGateway>{nullptr};
        }
    };
}// namespace dojoystick

#endif//JOYSTICK_GATEWAY_FACTORY_H
