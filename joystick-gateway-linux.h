#ifdef __linux__

#ifndef JOYSTICK_GATEWAY_LINUX_H
#define JOYSTICK_GATEWAY_LINUX_H

#include "joystick-gateway.h"

namespace dojoystick {
    class JoystickGatewayLinux : public JoystickGateway {
    public:
        explicit JoystickGatewayLinux(uint8_t joystickIndex);
        void startEventLoop() override;
        ~JoystickGatewayLinux() override;

    protected:
        bool openJoystick(uint8_t joystickIndex);
        void closeJoystick();

    private:
        uint8_t joystickIndex;
        int joy_fd;
    };
}// namespace dojoystick

#endif//JOYSTICK_GATEWAY_LINUX_H

#endif