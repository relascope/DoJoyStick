//
// Created by guru on 28.10.23.
//

#ifndef DOJOYSTICK_SETTINGS_H_
#define DOJOYSTICK_SETTINGS_H_

namespace dojoystick {
    class Parameters {
    public:
        constexpr static uint8_t kVelocity = 0;
        constexpr static uint8_t kMaxButtonCount = 32;
        constexpr static uint8_t kParameterCount = kMaxButtonCount + 1;
    };

    struct Settings {
        Settings() {
            // Notes which work good with x42 Black Pearl Drumkit
            // and Microsoft Sidewinder Precision Pro
            midiNoteForButtonNumber[4] = 49;
            midiNoteForButtonNumber[5] = 48;
            midiNoteForButtonNumber[6] = 36;
            midiNoteForButtonNumber[7] = 42;
            midiNoteForButtonNumber[8] = 41;
        }

        uint8_t velocity = 108;
        uint8_t joyStickIndex = 0;
        uint8_t midiNoteForButtonNumber[Parameters::kMaxButtonCount]{0};
    };


}// namespace dojoystick

#endif//DOJOYSTICK__SETTINGS_H_
