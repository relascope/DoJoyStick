//
// Created by guru on 11.10.23.
//

#ifndef DOJOYSTICK_JOYSTICKMIDIMEDIATOR_H
#define DOJOYSTICK_JOYSTICKMIDIMEDIATOR_H

#include "DPF/distrho/DistrhoDetails.hpp"
#include "DPF/distrho/extra/Thread.hpp"
#include "dojoystick-settings.h"
#include "joystick-gateway.h"
#include <algorithm>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace dojoystick {

    class DoJoyStickThread : public DISTRHO::Thread {};

    class MidiObserver {
    public:
        virtual void onMidiEvent(const MidiEvent &midiEvent) = 0;
        virtual ~MidiObserver() = default;
    };

    /** JoyStickMidiMediator
     * is the connection from the joystick to MIDI
     * it also runs the thread that reads the joystick and
     * notifies the observers about the events.
     *
     * DISTRHO::Thread is used for the thread. (there were problems with std::thread)q
    */
    class JoystickMidiMediator final
        : public JoystickObserver,
          public std::enable_shared_from_this<JoystickMidiMediator>,
          public DISTRHO::Thread {
    public:
        explicit JoystickMidiMediator(Settings &settings);

        ~JoystickMidiMediator() override;

        void addObserver(MidiObserver *const observer) { observers.push_back(observer); };

        void removeObserver(MidiObserver *const observer) {
            auto predicate = [&observer](const MidiObserver *elem) { return elem == observer; };
            observers.erase(std::remove_if(observers.begin(), observers.end(), predicate), observers.end());
        };

        void onJoystickEvent(const JoystickEvent &event) override;

    protected:
        // from DISTRHO::Thread
        void run() override { joystickGateway->startEventLoop(); }

        void notifyObservers(const MidiEvent &midiEvent) const {
            for (const auto observer: observers) { observer->onMidiEvent(midiEvent); }
        }

        void sendMidiMessage(const unsigned char *msg, size_t size) const;

        Settings &settings;
        std::unique_ptr<JoystickGateway> joystickGateway;
        std::thread joystickThread;

    private:
        std::vector<MidiObserver *> observers;
    };
}// namespace dojoystick
#endif//DOJOYSTICK_JOYSTICKMIDIMEDIATOR_H
