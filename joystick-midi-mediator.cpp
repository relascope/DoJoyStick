//
// Created by guru on 11.10.23.
//

#include "joystick-midi-mediator.h"
#include "DPF/distrho/DistrhoDetails.hpp"
#include "joystick-gateway-factory.h"
#include "joystick-gateway.h"
#include <iostream>

namespace dojoystick {
    void JoystickMidiMediator::onJoystickEvent(const JoystickEvent &event) {

        std::cout << "JoystickEvent: button " << std::to_string(event.buttonNumber) << " " << (event.buttonDown ? "down" : "up") << '\n';

        if (event.buttonNumber >= dojoystick::Parameters::kMaxButtonCount) return;

        unsigned char msg[3];
        msg[0] = event.buttonDown == 1 ? 0x90 : 0x80;
        msg[2] = settings.velocity;
        msg[1] = settings.midiNoteForButtonNumber[event.buttonNumber];
        sendMidiMessage(msg, sizeof(msg));
    }

    JoystickMidiMediator::JoystickMidiMediator(Settings &settings)
        : settings{settings}, DISTRHO::Thread(
                                      "DoJoyStick-Joystick-Thread") {

        this->settings = settings;

        joystickGateway = JoystickGatewayFactory::create(settings.joyStickIndex);

        joystickGateway->addJoystickObserver(this);
    }

    void JoystickMidiMediator::sendMidiMessage(const unsigned char *msg, const size_t size) const {
        if (size == 0) {
            std::cout << "DoJoyStick: cannot send with ZERO size\n";
            return;
        }

        MidiEvent event = {};
        event.size = size;
        for (size_t i = 0; i < size; ++i) { event.data[i] = msg[i]; }

        notifyObservers(event);
    }

    JoystickMidiMediator::~JoystickMidiMediator() {
        joystickGateway->stop();

        if (this->isThreadRunning()) {
            this->signalThreadShouldExit();
            this->stopThread(0);
        }
    }
}// namespace dojoystick
