DoJoy-Stick
========================

* Use Joystick as a Midi-Device
* can be used with jack(standalone) or as an audio plugin
* clap lv2 vst2 vst3 - plugin formats

Build
========================

    apt update --yes
    apt install cmake make libjack-jackd2-dev --yes

    git clone & cd into dir

    git submodule update --init --recursive
    mkdir build
    cd build
    cmake ..
    make

Run
========================

JACK Audio Connection Kit Standalone

    ./DoJoyStick

Or use the generated plugins in your favourite DAW (Digital Audio Workstation like Ardour, Cubase,...).     
You find them in the build/bin folder.  
You may have to copy them to the specified folder for the plugin type. 


Status
========================

- Joystick sends Midi data (configurable only through sourcecode at the moment - @see JoystickMediator.cpp).
  Usecase Joystick Microsoft SideWinder Precision Pro (USB) as a foot-pedal with x42 Black Pearl Drumkit

- Only one JoyStick is supported at the moment. => The first JoyStick, that is recognised by the system is used.  
- JoyStick has to be present, before the program starts. Hot-Plugging is not supported.  


Planned
========================

- configuration through UI (possible when using DoJoyStick as plugin)
- generalize joystick events (send osc, send keyboard event, open program,...)



Developer Notes
========================

Sooperlooper OSC commands (some of them)
http://essej.net/sooperlooper/doc_osc.html

### projects worth mentioning

MidiJoystick: could not be compiled on modern ubuntu22.04
QJoyStick: use JoyStick with Qt Library (uses SDL)  
SDL2 JoyStick works well in standalone (Jack) mode, but not in a plugin (vst2, vst3) - it does not catch all events, therefore we won't use SDL2.  

### interesting packages

    joystick evtest inputattach

