//
// Created by guru on 19.12.23.
//

#ifndef JOYSTICK_OBSERVER_H
#define JOYSTICK_OBSERVER_H

namespace dojoystick {
    class JoystickObserver {
    public:
        virtual void onJoystickEvent(const JoystickEvent &ev) = 0;
        virtual ~JoystickObserver() = default;
    };
}// namespace dojoystick

#endif//JOYSTICK_OBSERVER_H
