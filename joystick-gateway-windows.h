//
// Created by User on 12/20/2023.
//

#ifndef DOJOYSTICK_JOYSTICKGATEWAYWINDOWS_H
#define DOJOYSTICK_JOYSTICKGATEWAYWINDOWS_H

#include "joystick-gateway.h"
#include <cstdint>
namespace dojoystick {

    class JoystickGatewayWindows : public JoystickGateway {
    public:
        explicit JoystickGatewayWindows(uint8_t joystickIndex) : joystickIndex{joystickIndex} {}
        void startEventLoop() override;

    private:
        uint8_t joystickIndex;
    };

}// namespace dojoystick

#endif//DOJOYSTICK_JOYSTICKGATEWAYWINDOWS_H
