//
// Created by guru on 11.10.23.
//

#ifndef DOJOYSTICK_JOYSTICKEVENT_H
#define DOJOYSTICK_JOYSTICKEVENT_H
#include <cstdint>

namespace dojoystick {
    struct JoystickEvent {
        bool buttonDown;
        uint8_t buttonNumber;
    };
}// namespace dojoystick

#endif//DOJOYSTICK_JOYSTICKEVENT_H
